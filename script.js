document.addEventListener("DOMContentLoaded", function() {
    var checkbox = document.getElementById("toggle");
    var showAnnually = document.querySelectorAll(".show");
    var hideMonthly = document.querySelectorAll(".hide");
  
    function toggleDisplay() {
      if (checkbox.checked) {
        hideMonthly.forEach(function(element) {
          element.style.display = "block";
        });
        showAnnually.forEach(function(element) {
          element.style.display = "none";
        });
      } else {
        hideMonthly.forEach(function(element) {
          element.style.display = "none";
        });
        showAnnually.forEach(function(element) {
          element.style.display = "block";
        });
      }
    }
  
    function handleCheckboxChange() {
      toggleDisplay();
    }
  
    function handleCheckboxKeydown(event) {
      if (event.code === "Space" || event.code === "Enter") {
        checkbox.checked = !checkbox.checked;
        toggleDisplay();
      }
    }
  
    checkbox.addEventListener("change", handleCheckboxChange);
    checkbox.addEventListener("keydown", handleCheckboxKeydown);

    document.getElementById('colorButton').addEventListener('click', changeBackgroundColor);

    function changeBackgroundColor() {
    const colors = [
      'hsl(240, 78%, 98%)',
      'hsl(234, 14%, 74%)',
      'hsl(233, 13%, 49%)',
      'hsl(232, 13%, 33%)'
    ];

    const randomIndex = Math.floor(Math.random() * colors.length);
    const selectedColor = colors[randomIndex];
  
    document.body.style.transition = 'background 0.5s'; 
    document.body.style.backgroundColor = selectedColor;
    }
    
  });
  